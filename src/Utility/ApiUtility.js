import ErrorMessages from "../Configuration/ErrorMessages";

export default class ApiUtility {
  /**
   * The base url for the API.
   */
  static baseUrl = "https://api.cards.kaderli.dev/";

  /**
   *
   * @param {String} endpoint
   * @param {String} method
   * @param {Object} properties
   * @param {Function} callback
   */
  static async call(endpoint, method = "GET", properties = null, callback = () => {}) {
    let body = null;

    if (properties) {
      body = new URLSearchParams();
      for (let property in properties) {
        body.set(property, properties[property]);
      }
    }

    const response = await fetch(this.baseUrl + endpoint, {
      method,
      body,
      cache: "no-store",
    });

    const jsonResponse = await response.json();
    if (jsonResponse.status_code && jsonResponse.status_code == 500) {
      /*return Dialog.alert({
        title: ErrorMessages[jsonResponse.message].title,
        message: ErrorMessages[jsonResponse.message].message,
        confirmText: "OK",
      });*/
    }

    if (callback) {
      callback(jsonResponse);
    }

    return jsonResponse;
  }

  static showError(errorCode) {
    /*return Dialog.alert({
      title: ErrorMessages[errorCode].title,
      message: ErrorMessages[errorCode].message,
      confirmText: "OK",
    });*/
  }
}
