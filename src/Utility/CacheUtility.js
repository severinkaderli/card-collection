/**
 * This is an utility class for managing caching of items
 * using the HTML 5 Local Storage API.
 *
 * @export
 * @class CacheUtility
 */
export default class CacheUtility {
  /**
   * The name of the local storage key.
   */
  static cacheName = "card-collection-cache";

  /**
   * Restores the data from the cache.
   *
   * @returns
   * @memberof CacheUtility
   */
  static restore(cacheKey) {
    let cache = {};
    if (window.localStorage.getItem(this.cacheName)) {
      cache = JSON.parse(window.localStorage.getItem(this.cacheName));
    }

    if (cache[cacheKey]) {
      return cache[cacheKey];
    }
  }

  /**
   * Saves the data to the cache.
   *
   * @param {Object} object
   * @memberof CacheUtility
   */
  static save(cacheKey, data) {
    let cache = {};
    if (window.localStorage.getItem(this.cacheName)) {
      cache = JSON.parse(window.localStorage.getItem(this.cacheName));
    }

    cache[cacheKey] = data;
    window.localStorage.setItem(this.cacheName, JSON.stringify(cache));
  }
}
