import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";

import ElementPlus from "element-plus";

import "element-plus/dist/index.css";
import "element-plus/theme-chalk/dark/css-vars.css";

import locale from "element-plus/dist/locale/en.js";

import App from "./App.vue";

import CardCollection from "./Components/CardCollection.vue";

const routes = [
  {
    path: "/",
    redirect: "/yugioh/cards",
  },
  {
    path: "/yugioh/cards",
    component: CardCollection,
    props: {
      gameId: 2,
      name: "Yu-Gi-Oh!",
      defaultLanguage: 1,
      setCodeLength: 6,
    },
  },
  {
    path: "/magic/cards",
    component: CardCollection,
    props: {
      gameId: 1,
      name: "Magic the Gathering",
      defaultLanguage: 2,
      setCodeLength: 5,
    },
  },
  {
    path: "/duelmasters/cards",
    component: CardCollection,
    props: {
      gameId: 3,
      name: "Duel Masters",
      defaultLanguage: 1,
      setCodeLength: 7,
    },
  },
  {
    path: "/pokemon/cards",
    component: CardCollection,
    props: {
      gameId: 4,
      name: "Pokémon Trading Card Game",
      defaultLanguage: 1,
      setCodeLength: 6,
    },
  },
];

const router = createRouter({
  history: createWebHistory("/"),
  routes,
});

const app = createApp(App);
app.use(router);
app.use(ElementPlus, { locale });
app.mount("#app");
