const Configuration = {
  /**
   * The key for the cache of the Yu-Gi-Oh! cards.
   *
   * @type {String}
   */
  YGO_CACHE_KEY: "ygo-cards",
  /**
   * The key for the cache of the Magic The Gathering cards.
   *
   * @type {String}
   */
  MTG_CACHE_KEY: "mtg-cards",
  /**
   * The key for the cache of the Duel Masters cards.
   *
   * @type {String}
   */
  DM_CACHE_KEY: "dm-cards",
  /**
   * The key for the cache of the Pokémon cards.
   *
   * @type {String}
   */
  PKMN_CACHE_KEY: "pkmn-cards",
};

export default Configuration;
