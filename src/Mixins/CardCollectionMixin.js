import CardCollection from "../Components/CardCollection";
import apiUtility from "../Utility/ApiUtility";
import cacheUtility from "../Utility/CacheUtility";

export default {
  components: {
    CardCollection,
  },
  props: ["gameId"],
  data() {
    return {
      cards: [],
      apiUtility,
      cacheUtility,
    };
  },
  created() {
    this.fetchCards();
  },
  methods: {
    async fetchCards() {
      this.cards = await this.apiUtility.call(`cards?game=${this.gameId}`);
    },
    updateData() {
      this.fetchCards();
    },
  },
};
